﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using lab_7_kpz_2.Models;
using System.Net.Http;

namespace lab_7_kpz_2.Controllers
{
    public class CRUDController : Controller
    {
        // GET: CRUD
        public ActionResult Index()
        {
            IEnumerable<Client> clobj = null;
            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44381/api/ClCrud");

            var consumeapi = hc.GetAsync("ClCrud");
            consumeapi.Wait();

            var readdata = consumeapi.Result;
            if (readdata.IsSuccessStatusCode)
            {
                var displaydata = readdata.Content.ReadAsAsync<IList<Client>>();
                displaydata.Wait();

                clobj = displaydata.Result;

            }
            return View(clobj);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Client inserttemp)
        {
            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44381/api/ClCrud");

            var insertrecord = hc.PostAsJsonAsync<Client>("ClCrud", inserttemp);

            insertrecord.Wait();
            var savedata = insertrecord.Result;
            if (savedata.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            return View("Create");
        }

        public ActionResult Details(int id)
        {
            ClClass clobj = null;

            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44381/api/");

            var consumeapi = hc.GetAsync("ClCrud?Id=" + id.ToString());
            consumeapi.Wait();

            var readdata = consumeapi.Result;
            if (readdata.IsSuccessStatusCode)
            {
                var displaydata = readdata.Content.ReadAsAsync<ClClass>();
                displaydata.Wait();
                clobj = displaydata.Result;
            }
            return View(clobj);
        }

        public ActionResult Edit(int id)
        {
            ClClass clobj = null;

            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44381/api/");

            var consumeapi = hc.GetAsync("ClCrud?Id=" + id.ToString());
            consumeapi.Wait();

            var readdata = consumeapi.Result;
            if (readdata.IsSuccessStatusCode)
            {
                var displaydata = readdata.Content.ReadAsAsync<ClClass>();
                displaydata.Wait();
                clobj = displaydata.Result;
            }
            return View(clobj);
        }

        [HttpPost]
        public ActionResult Edit(ClClass cc)
        {
            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44381/api/ClCrud");

            var insertrecord = hc.PutAsJsonAsync<ClClass>("ClCrud", cc);
            insertrecord.Wait();

            var savedata = insertrecord.Result;
            if (savedata.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.message = "Client record not updated";
            }
            return View(cc);
        }

        public ActionResult Delete(int id)
        {
            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44381/api/ClCrud");

            var delrecord = hc.DeleteAsync("ClCrud/" + id.ToString());
            delrecord.Wait();

            var displaydata = delrecord.Result;
            if (displaydata.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            return View("Index");
        }
    }
}