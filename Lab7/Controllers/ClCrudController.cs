﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using lab_7_kpz_2.Models;

namespace lab_7_kpz_2.Controllers
{
    public class ClCrudController : ApiController
    {
        IceRinkEntities5 ir = new IceRinkEntities5();
        public IHttpActionResult getcl()
        {
            var results = ir.Clients.ToList();
            return Ok(results);
        }

        [HttpPost]
        public IHttpActionResult clinsert(Client clinsert)
        {
            ir.Clients.Add(clinsert);
            ir.SaveChanges();
            return Ok();
        }

        public IHttpActionResult GetClid(int id)
        {
            ClClass cldetails = null;
            cldetails = ir.Clients.Where(x => x.Id == id).Select(x => new ClClass
            {
                Id = x.Id,
                Name = x.Name,
                DiscountCardId = x.DiscountCardId,
            }).FirstOrDefault<ClClass>();
            if (cldetails == null)
            {
                return NotFound();
            }
            return Ok(cldetails);
        }

        public IHttpActionResult Put(ClClass cc)
        {
            var updatecl = ir.Clients.Where(x => x.Id == cc.Id).FirstOrDefault<Client>();
            if(updatecl != null)
            {
                updatecl.Id = cc.Id;
                updatecl.Name = cc.Name;
                updatecl.DiscountCardId = cc.DiscountCardId;
                ir.SaveChanges();                
            }
            else
            {
                return NotFound();
            }
            return Ok();
        }

        public IHttpActionResult Delete(int id)
        {
            var cldel = ir.Clients.Where(x => x.Id == id).FirstOrDefault();
            ir.Entry(cldel).State = System.Data.Entity.EntityState.Deleted;
            ir.SaveChanges();
            return Ok();
        }
    }
}
