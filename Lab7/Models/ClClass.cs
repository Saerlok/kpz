﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace lab_7_kpz_2.Models
{
    public class ClClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? DiscountCardId { get; set; }
    }
}