﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab_3_kpz
{
    public class Client {
        public int clID { get; set; }
        public string clName { get; set; }

        public override string ToString()
        {
            return string.Format("Client ID: {0}, client name: {1}",
                clID, clName);
        }
    }
}
