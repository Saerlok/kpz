﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab_3_kpz
{
    public class Employee {
        public int empID { get; set; }
        public string empName { get; set; }
        public string empProffecion { get; set; }
    }
}
