﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace lab_3_kpz
{
    public class ModelTests
    {

        public static List<Client> Сlients
        {
            get
            {
                return new List<Client>()
                {
                    new Client() { clID = 1, clName = "El Fernando" },
                    new Client() { clID = 2, clName = "Greg Grifaello" },
                    new Client() { clID = 3, clName = "Snoop" },
                    new Client() { clID = 4, clName = "Old Man" },
                    new Client() { clID = 5, clName = "Hatake Kakashi" },
                    new Client() { clID = 6, clName = "Klint Eastwood" },
                    new Client() { clID = 7, clName = "Charlie B. Brooklin" },
                    new Client() { clID = 8, clName = "Francisco" },
                    new Client() { clID = 9, clName = "Shrek" }
                };
            }
        }

        public static List<Employee> Employees
        {
            get
            {
                return new List<Employee>()
                {
                    new Employee { empID = 1, empName = "Adolf Friderikh", empProffecion = "seller" },
                    new Employee { empID = 2, empName = "Joseff Manuar", empProffecion = "manager" },
                    new Employee { empID = 3, empName = "Alcoln Linqoln", empProffecion = "manager" },
                    new Employee { empID = 4, empName = "Josephine Smith", empProffecion = "seller" },
                    new Employee { empID = 5, empName = "Phantom Dancer", empProffecion = "seller" },
                    new Employee { empID = 6, empName = "Leonelya Messi", empProffecion = "seller" }
                };
            }
        }

        public static List<Booking> Bookings
        {
            get
            {
                return new List<Booking>()
                {
                    new Booking { bookID = 1, clID = 1, clName = "El Fernando", empID = 1,
                        bookType = "tripleRide", bookPrice = 120 },
                    new Booking { bookID = 2, clID = 3, clName = "Snoop", empID = 2,
                        bookType = "doubleRide", bookPrice = 70 },
                    new Booking { bookID = 3, clID = 8, clName = "Francisco", empID = 8,
                        bookType = "standart", bookPrice = 40 },
                    new Booking { bookID = 4, clID = 1, clName = "El Fernando", empID = 2,
                        bookType = "tripleRide", bookPrice = 120 },
                    new Booking { bookID = 5, clID = 6, clName = "Klint Eastwood", empID = 8,
                        bookType = "doubleRide", bookPrice = 70 },
                    new Booking { bookID = 6, clID = 9, clName = "Shrek", empID = 3,
                        bookType = "doubleRide", bookPrice = 70 }
                };
            }
        }
    }
}
