﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab_3_kpz
{
    public class Booking {
        public int bookID { get; set; }
        public int clID { get; set; }
        public string clName { get; set; }
        public int empID { get; set; }
        public string bookType { get; set; }
        public int bookPrice { get; set; }

        /*public Booking(int bID = 0,
            int cID = 0,
            string cName = "no name",
            int eID = 0,
            string bType = "no type",
            int bPrice = 0)
        {
            bookID = bID;
            clID = cID;
            clName = cName;
            empID = eID;
            bookType = bType;
            bookPrice = bPrice;
        }*/
        public override string ToString()
        {
            return string.Format("booking ID: {0}, client ID: {1}, client name{2}, " +
                "employee ID: {3}, booking type: {4}, booking price: {5}",
                bookID, clID, clName, empID, bookType, bookPrice);
        }
    } 
}
