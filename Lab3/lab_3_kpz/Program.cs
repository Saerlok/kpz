﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace lab_3_kpz
{
    class Program
    {

        static void Main(string[] args)
        {
            QueryStringArray();

            // Get the array returned and print it
            int[] intArray = QueryIntArray();

            foreach (int i in intArray)
                Console.WriteLine(i);

            Console.WriteLine();

            QueryList();

            QueryCollection();

            QueryOther();

            Console.ReadLine();

        }

        static void QueryStringArray() // select, where, sort
        {
            string[] clients = {"El Fernando", "Greg Grifaello",
            "Snoop", "Old Man", "Hatake Kakashi",
            "Klint Eastwood", "Charlie B. Brooklin", "Francisco",
            "Shrek"};

            var clientsWithSpaces = from cl in clients
                                    where cl.Contains(" ")
                                    orderby cl descending
                                    select cl;

            foreach (var i in clientsWithSpaces)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine();

        }

        static int[] QueryIntArray() //list to array
        {
            int[] nums = new int[9];
            List<Client> cl = ModelTests.Сlients;
            int it = 0;
            foreach (var i in cl)
            {
                nums[it] = i.clID;
                it++;
            }
            var gt5 = from num in nums
                       where num > 5
                       orderby num
                       select num;

            foreach (var i in gt5)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine();

            var listGT5 = gt5.ToList<int>();
            var arrayGT5 = listGT5.ToArray();
            var arr = cl.ToArray();
            //var arrayGT20 = gt20.ToArray();

            return arrayGT5;

        }

        static void QueryList() // list, sort on name, comparer, anonymous
        {           
            List<Employee> emp = ModelTests.Employees;
            var empEnum = emp.OfType<Employee>();

            var emSellers = from em in empEnum
                            where em.empProffecion == "seller"
                            orderby em.empName
                            select em;

            foreach (var em in emSellers)
            {
                Console.WriteLine("{0} {1} is {2}",
                    em.empID, em.empName, em.empProffecion);
            }
            
            Console.WriteLine();

            //comparer
            var result = emp.OrderBy(em => em,
                Comparer<Employee>.Create((e1, e2) =>
                {
                    return e1.empName.CompareTo(e2.empName);
                }));

            foreach (var em in result)
            {
                Console.WriteLine("{0} {1} is {2}",
                    em.empID, em.empName, em.empProffecion);
            }

            Console.WriteLine();

            //anonymous class
            var v = new { empID = 4, empName = "Sally Salivan", empProffecion =  "manager"};
        
            var emManagers = from em in empEnum
                            where em.empProffecion == "manager" || em.empID == v.empID
                            select new { em.empName, em.empProffecion }; 
            
            foreach (var em in emManagers)
            {
                Console.WriteLine("Employee Name: {0} is {1}", 
                    em.empName, em.empProffecion);
            }
            Console.WriteLine();
        }

        static void QueryCollection()  // dictionary, ToString()/ToFormat()
        {
            Dictionary<int, Booking> bookings = new Dictionary<int, Booking>(6);
            List<Booking> bList = ModelTests.Bookings;
            foreach(var b in bList)
            {
                bookings.Add(b.bookID, b);
            }
            foreach (var b in bookings)
            {
                Console.WriteLine("Key:{0} is booking ID, Client: {1}, Price: {2}", b.Key, b.Value.clName,
                    b.Value.bookPrice);
            }
            Console.WriteLine();

            var bigBookings = from booking in bookings
                          where (booking.Key >= 2) && (booking.Value.bookPrice >= 70)
                          orderby booking.Key
                          select booking;

            foreach (var b in bigBookings)
            {
                Console.WriteLine("A {0} booking key just for {1}$ for mister {2}",
                    b.Key, b.Value.bookPrice, b.Value.clName);
            }

            Console.WriteLine();
            // ToString()
            foreach (var b in bigBookings)
            {
                Console.WriteLine(b.ToString());
            }
            Console.WriteLine();
        }

        static void QueryOther() // SortedList, Format() // Joins // grouping
        {
            List<Client> clients = ModelTests.Сlients;
            SortedList sortedClients = new SortedList();
            foreach(var c in clients)
            {
                sortedClients.Add(c.clName, c);
            }
            Console.WriteLine("Count:    {0}", sortedClients.Count);
            for (int i = 0; i < sortedClients.Count; i++)
            {
                Console.WriteLine("{0}:\t{1}", sortedClients.GetKey(i), sortedClients.GetByIndex(i).ToString());
            }
            Console.WriteLine();

            List<Booking> bookings = ModelTests.Bookings;
            List<Employee> employees = ModelTests.Employees;

            var innerJoin =
               from c in clients
               join b in bookings on c.clID
               equals b.bookID
               select new { ClientName = c.clName, BookingID = b.bookID };

            foreach (var i in innerJoin)
            {
                Console.WriteLine("{0} in booking with ID: {1}",
                    i.ClientName, i.BookingID);
            }

            Console.WriteLine();

            var groupJoin = // Group join/ shows amount of Bookings for each Client 
                from cl in clients
                orderby cl.clName
                join bk in bookings on cl.clName
                equals bk.clName into Group1
                select new
                {
                    Client = cl.clName,
                    Booking = from cl2 in Group1
                              orderby cl2.clName
                              select cl2
                };

            int totalBk = 0;

            foreach (var Group1 in groupJoin)
            {
                Console.WriteLine(Group1.Client);
                foreach (var bk in Group1.Booking)
                {
                    totalBk++;
                    Console.WriteLine("* {0}", bk.clName);
                }
            }
            Console.WriteLine();
        }
    }
}
