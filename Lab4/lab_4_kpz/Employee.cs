﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab_4_kpz
{
    //множинне наслідування
    abstract class Employee : ITemplateEmployee
    {
        public string name { get; set; }
        public int salary { get; set; }
        public string position { get; set; }
        public Employee(string name1, int salary1, string position1) {
            name = name1;
            position = position1;
            salary = salary1;
        }
        public void DoSmthg() 
        { 
        
        }
    }
}
