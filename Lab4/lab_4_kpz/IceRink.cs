﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab_4_kpz
{
    //ініціалізації полів як статичних так і динамічних
    static class IceRink
    {
        //внутрішній клас з доступом меншим за public
        static private int numOfIceRink;
        static private int revenue;
        public class CashRegisters {
            public List<Cashier> cashiers;
            private int numOfRegister;
        }
        //перевантаження функції
        static public int CountWorkers(CashRegisters CR) {
            return 0;//CR.cashiers.Count;
        }
        static public int CountWorkers(List<CashRegisters> LRC) {
            //int num = 0;
            //foreach(var i in LRC)
            //{
            //    num += i.cashiers.Count; 
            //}
            return 0;//num;
        }
        private class Rink
        {
            public List<Cashier> cashiers;
            private int numOfRegister;
        }
    }
}
