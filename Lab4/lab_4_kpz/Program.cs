﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace lab_4_kpz
{
    class Program
    {
        static void Main(string[] args)
        {
            // ланцюжок наслідування у якому б був звичайний клас, абстрактний клас та інтерфейс
            Coache coache = new Coache("Leanelya Mess", 1200, "high", 6);  
            Console.WriteLine(coache.name);

            //булівські операції на перелічуваних типах(^,||, &&. &, |,…) // pobitovi
            dayOfWork workDay = dayOfWork.Tuesday | dayOfWork.Wednesday | dayOfWork.Thursday;
            dayOfWork workDay1 = dayOfWork.Friday & dayOfWork.Saturday | dayOfWork.Monday;
            bool workOnTuesday = (workDay | dayOfWork.Monday) == dayOfWork.Sunday;
            bool workOnTuesday2 = ((workDay1 | dayOfWork.Wednesday) & dayOfWork.Saturday) == dayOfWork.Saturday;
            Console.WriteLine("Work on thuesday? {0}, {1}", workOnTuesday, workOnTuesday2);
            Console.WriteLine();

            //перевантаження
            IceRink.CashRegisters cr = new IceRink.CashRegisters();
            IceRink.CountWorkers(cr);
            List<IceRink.CashRegisters> crl = new List<IceRink.CashRegisters>();
            IceRink.CountWorkers(crl);

            //Task11 - method and static method
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            for(int i = 0; i < 100000000; i++)
            {
                VIPClient.MakeOrder();
            }
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}.{1:00}",
            ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("RunTime for static method " + elapsedTime);

            Client client = new Client();
            stopWatch = Stopwatch.StartNew();
            for (int i = 0; i < 100000000; i++)
            {
                client.MakeOrder();
            }
            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}.{1:00}",
            ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("RunTime for ordinary method " + elapsedTime);

            //Dostup do metodiv
            stopWatch = Stopwatch.StartNew();
            for (int i = 0; i < 100000000; i++)
            {
                client.OrderSmth();
            }
            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}.{1:00}",
            ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("Access RunTime for ordinary method " + elapsedTime);

            stopWatch = Stopwatch.StartNew();
            for (int i = 0; i < 100000000; i++)
            {
                Client.OrderSmth1();
            }
            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}.{1:00}",
            ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("Access RunTime for static method " + elapsedTime);

            //static and ord vars
            stopWatch = Stopwatch.StartNew();
            for (int i = 0; i < 100000; i++)
            {
                Client.name1 += "+";
            }
            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}.{1:00}",
            ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("RunTime for static var " + elapsedTime);

            stopWatch = Stopwatch.StartNew();
            for (int i = 0; i < 100000; i++)
            {
                client.name += "+";
            }
            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}.{1:00}",
            ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("RunTime for ordinary var " + elapsedTime);

            ////////// boxing unboxing
            List<object> mixedList = new List<object>();

            mixedList.Add("Work days: ");
            mixedList.Add(dayOfWork.Monday);
            mixedList.Add(dayOfWork.Friday);

            mixedList.Add("Weekends: ");
            mixedList.Add(dayOfWork.Sunday);
            for (int j = 5; j < 10; j++)
            {
                mixedList.Add(j);
            }
            foreach (var item in mixedList)
            {
                Console.WriteLine(item);
            }
            var testSum = 0;
            for (var j = 5; j < 10; j++)
            {
                testSum += (int)mixedList[j] * (int)mixedList[j];
            }
            Console.WriteLine("Sum: " + testSum);

            Console.ReadLine();
        }
    }
}
