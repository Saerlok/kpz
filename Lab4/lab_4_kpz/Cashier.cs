﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab_4_kpz
{
    class Cashier : Employee
    {
        private int cashRegister;
        //перевантаження конструкторів базового класу та поточного класу
        // використання ключових слів base та this
        public Cashier(string name1, int salary1, string position1, int cashRegister1) : 
            base(name1, salary1, position1) 
        {
            cashRegister = cashRegister1;
        }
        public void Calculate() 
        {
            Console.WriteLine("Total price was calculated");
        }
    }

    //перелічуваний тип
    enum dayOfWork 
    {
        Monday = 1,     //0001
        Tuesday = 2,    //0010
        Wednesday = 4,  //0100
        Thursday = 8,   //1000
        Friday = 9,     //1001
        Saturday = 12,  //1100
        Sunday = 15     //1110
    }

}
