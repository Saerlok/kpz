﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace kpz_6
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            /*using(var context = new MyDBContext())
            {
                var client = new Client()
                {
                    Name = "Bobby"                    
                };
                var client2 = new Client()
                {
                    Name = "Franky",
                    DiscountCardId = 4
                };

                context.Clients.Add(client);
                context.Clients.Add(client2);
                context.SaveChanges();

                var bookings = new List<Booking>()
                {
                    new Booking() { Date = DateTime.Today, ClientId = client2.Id },
                    new Booking() { Date = DateTime.Now, ClientId = client2.Id},
                    new Booking() { Date = DateTime.Today, ClientId = client.Id}
                };
                //context.Bookings.AddRange(bookings);
                //context.Bookings.RemoveRange(context.Bookings);
                context.SaveChanges();

                //context.Bookings.RemoveRange(bookings);
                
            }*/
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new MyDBContext())
            {
                listBo.Items.Clear();
                
                foreach (var cl in context.Clients)
                {
                    if (clNameBooking.Text == cl.Name)
                    {
                        var bo = new Booking()
                        {
                            ClientId = cl.Id,
                            MinutesOfRide = Convert.ToInt32(timeOfRide.Text),
                            Date = DateTime.Now
                        };
                        context.Bookings.Add(bo);
                    }
                }                
                context.SaveChanges();
                foreach (var booking in context.Bookings)
                {
                    listBo.Items.Add(booking.Client.Name + "   " + booking.Date+ "   "+ booking.MinutesOfRide);
                }
                //listCl.Items.Add($"Client name: {}, Date: {booking.Date}, Riding Miinutes: {booking.MinutesOfRide}")
            };
        }
    

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            using (var context = new MyDBContext())
            {
                listCl.Items.Clear();
                var client = new Client()
                {
                    Name = clNameAdd.Text,
                    DiscountCardId = Convert.ToInt32(clCardAdd.Text)
                };
                context.Clients.Add(client);
                context.SaveChanges();

                foreach (var cl in context.Clients)
                {
                    listCl.Items.Add(cl.Name +"   "+ cl.DiscountCardId);
                }
            };
            
            
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            using (var context = new MyDBContext())
            {
                listCl.Items.Clear();
                foreach (var cl in context.Clients)
                {
                    if (clDelete.Text == cl.Name)
                    {
                        context.Clients.Remove(cl);
                    }
                }
                context.SaveChanges();
                foreach (var cl in context.Clients)
                {
                    listCl.Items.Add(cl.Name + "   " + cl.DiscountCardId);
                }
            };
        }
    }
}
