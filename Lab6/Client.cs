﻿using System;
using System.Collections.Generic;

namespace kpz_6
{
    public class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? DiscountCardId { get; set; }

        public virtual ICollection<Booking> Bookings { get; set; }
    }
}
