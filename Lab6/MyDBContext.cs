﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kpz_6
{
    public class MyDBContext : DbContext
    {
        public MyDBContext() : base("DBConnectionString")
        {
        
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Booking> Bookings { get; set; }
    }
}
