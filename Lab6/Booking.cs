﻿using System;
using System.Collections.Generic;

namespace kpz_6
{
    public class Booking
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int ClientId { get; set; }
        public int MinutesOfRide { get; set; }

        public virtual Client Client { get; set; }
    }
}
