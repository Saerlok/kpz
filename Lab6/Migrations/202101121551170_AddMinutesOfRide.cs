﻿namespace kpz_6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMinutesOfRide : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bookings", "MinutesOfRide", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Bookings", "MinutesOfRide");
        }
    }
}
