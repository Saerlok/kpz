﻿using System;
using System.Collections;

namespace lab2_kpz
{
    class Program
    {
        static void Main(string[] args)
        {
            Player pl = new Player(5);
            Console.WriteLine($"Current lvl: {pl.Level}");
            pl.LvlEvent += DisplayMessage;
            pl.LvlEvent += DisplayRedMessage;
            pl.LvlUp(2);  
            Console.WriteLine($"Current lvl: {pl.Level}");
            pl.LvlEvent -= DisplayRedMessage;
            pl.LvlDown(3); 
            Console.WriteLine($"Current lvl: {pl.Level}");
            pl.ScoreEvent += DisplayMessage_2;
            pl.ScoreIncr(70);
            pl.ScoreDecr(40);
            pl.ScoreDecr(90);
            Console.Read();
        }
        private static void DisplayMessage(string message)
        {
            Console.WriteLine(message);
        }
        private static void DisplayRedMessage(String message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            Console.ResetColor();
        }
        private static void DisplayMessage_2(object sender, PlayerEventArgs e)
        {
            Console.WriteLine(e.Message);
            Console.WriteLine($"Score: {e.Score}");
        }
    }

    class Player
    {
        public delegate void LevelHandler(string message);
        public event LevelHandler LvlEvent;
        public Player(int lvl)
        {
            Level = lvl;
            Score = 0;
        }
        public int Level { get; private set; }
        public int Score { get; private set; }
        public void LvlUp(int lvl)
        {
            Level += lvl;
            LvlEvent?.Invoke($"Level up by: {lvl}");  
        }
        public void LvlDown(int lvl)
        {          
               Level -= lvl;
               LvlEvent?.Invoke($"Level down by: {lvl}");              
        }

        public delegate void ScoreHandler(object sender, PlayerEventArgs e);
        public event ScoreHandler ScoreEvent;
        public void ScoreIncr(int score)
        {
            Score += score;
            ScoreEvent?.Invoke(this, new PlayerEventArgs($"Player scored {score}", score));
        }
        public void ScoreDecr(int score)
        {
            if (Score >= score)
            {
                Score -= score;
                ScoreEvent?.Invoke(this, new PlayerEventArgs($"Your score decreased by {Score}", score));
            }
            else
            {
                Score = 0;
                score = 0;
                ScoreEvent?.Invoke(this, new PlayerEventArgs($"Your score now is {Score}", score)); ;
            }
        }
    }

    class PlayerEventArgs
    {
        public string Message { get; }
        public int Score { get; }

        public PlayerEventArgs(string mes, int score)
        {
            Message = mes;
            Score = score;
        }
    }

}
