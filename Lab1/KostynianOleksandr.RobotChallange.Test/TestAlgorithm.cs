﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace KostynianOleksandr.RobotChallange.Test
{
    [TestClass]
    public class TestAlgorithm
    {
        [TestMethod]
        public void TestIsStationFreeFunction()
        {
            var algorithm = new KostynianOleksandrAlgorithm();
            var map = new Map();
            var stationPosition = new Position(3, 1);
            map.Stations.Add(new EnergyStation() { Position = stationPosition, Energy = 200, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 700, Position = new Position(1, 1) } };
            var robot = new Robot.Common.Robot() { Energy = 200, Position = new Position(2, 2) };
            Position pos = new Position(3, 3);

            Assert.IsTrue(algorithm.IsStationFree(map.Stations[0], robot, robots));
        }

        [TestMethod]
        public void TestAttackEnemyRobotFunction()
        {
            var algorithm = new KostynianOleksandrAlgorithm();

            var myRobot = new Robot.Common.Robot()
            {
                Energy = 100,
                Position = new Position(1, 1)
            };
            var enemyRobot = new Robot.Common.Robot()
            {
                Energy = 800,
                Position = new Position(1, 3)
            };
           
            Position expPosition = algorithm.AttackEnemyRobot(myRobot, enemyRobot);
            Assert.AreEqual(expPosition, enemyRobot.Position);
        }

        [TestMethod]
        public void TestMoveToStationFunction()
        {
            var algorithm = new KostynianOleksandrAlgorithm();
            var map = new Map();
            var myRobot = new Robot.Common.Robot()
            {
                Energy = 300,
                Position = new Position(1, 1)
            };
            var stationPosition = new Position(5, 1);
            map.Stations.Add(new EnergyStation()
            {
                Position = stationPosition,
                Energy = 200,
                RecoveryRate = 2
            });
            stationPosition = new Position(4, 1);
            map.Stations.Add(new EnergyStation()
            {
                Position = stationPosition,
                Energy = 400,
                RecoveryRate = 2
            });
            Position actPosition = algorithm.MoveToStation(map.Stations[0], myRobot);
            Assert.AreEqual((map.Stations[0].Position.X + myRobot.Position.X) / 2, actPosition.X);

            actPosition = algorithm.MoveToStation(map.Stations[1], myRobot);
            Assert.AreEqual(map.Stations[1].Position, actPosition);
        }

        [TestMethod]
        public void TestFindNearestStation()
        {
            var algorithm = new KostynianOleksandrAlgorithm();
            var map = new Map();
            var stationPosition = new Position(3, 1);
            map.Stations.Add(new EnergyStation() { Position = stationPosition, Energy = 200, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 700, Position = new Position(1, 1) } };
            var robot = new Robot.Common.Robot() { Energy = 200, Position = new Position(2, 2) };

            Assert.AreEqual(stationPosition, algorithm.FindNearestFreeStation(robot, map, robots).Position);
        }

        [TestMethod]
        public void TestIsCellFreeFunction()
        {
            var algorithm = new KostynianOleksandrAlgorithm();
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 700, Position = new Position(1, 1) } };
            var robot = new Robot.Common.Robot() { Energy = 200, Position = new Position(2, 2) };
            Position pos = new Position(3, 3);

            Assert.IsTrue(algorithm.IsCellFree(pos, robot, robots));
        }

        [TestMethod]
        public void TestFindEnergizedStation()
        {
            var algorithm = new KostynianOleksandrAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(1, 1), Energy = 500, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Position = new Position(2, 2), Energy = 200, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Position = new Position(3, 3), Energy = 600, RecoveryRate = 2 });

            Assert.AreEqual(map.Stations[2], algorithm.FindEnergizedStation(map));
        }



        [TestMethod]
        public void TestFindStationToGo()
        {
            var algorithm = new KostynianOleksandrAlgorithm();
            var map = new Map();
            var robots = new List<Robot.Common.Robot>() { 
                new Robot.Common.Robot() { Energy = 700, Position = new Position(1, 1) },
                new Robot.Common.Robot() { Energy = 200, Position = new Position(2, 4) },
                new Robot.Common.Robot() { Energy = 5200, Position = new Position(3, 2) }
            };
            map.Stations.Add(new EnergyStation() { Position = new Position(1, 1), Energy = 800, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Position = new Position(2, 2), Energy = 200, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Position = new Position(3, 3), Energy = 600, RecoveryRate = 2 });
            EnergyStation bestStation = algorithm.FindStationToGo(robots[1], map, robots);
            Assert.AreEqual(map.Stations[2].Position, bestStation.Position);
        }

        [TestMethod]
        public void TestFindRobotToAttack()
        {
            var algorithm = new KostynianOleksandrAlgorithm();
            var map = new Map();
            Robot.Common.Robot bot = new Robot.Common.Robot()
            {
                Energy = 200,
                Position = new Position(1, 3)
            };
            Robot.Common.Robot bot2 = new Robot.Common.Robot();
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 400,
                Position = new Position(2, 3) },
                new Robot.Common.Robot() { Energy = 5200, Position = new Position(2, 6) } };

            bot2 = algorithm.FindRobotToAttack(bot, map, robots);
            Assert.AreEqual(bot2.Position, robots[1].Position);
        }

        [TestMethod]
        public void TestAlgorithmScenarioAttackPreferance()
        {
            var algorithm = new KostynianOleksandrAlgorithm();
            var map = new Map();
            var robots = new List<Robot.Common.Robot>() {
                new Robot.Common.Robot() { Energy = 700, Position = new Position(1, 1) },
                new Robot.Common.Robot() { Energy = 200, Position = new Position(2, 4) },
                new Robot.Common.Robot() { Energy = 10000, Position = new Position(3, 2) }
            };
            map.Stations.Add(new EnergyStation() { Position = new Position(1, 1), Energy = 800, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Position = new Position(2, 2), Energy = 50, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Position = new Position(3, 3), Energy = 100, RecoveryRate = 2 });
            var act =  algorithm.DoStep(robots, 1, map);
            var exp = new MoveCommand() { NewPosition = robots[2].Position,
                Description = "robot attacked"
            };
            Assert.IsTrue(act is MoveCommand);
            Assert.AreEqual(exp.Description, act.Description);
        }

        [TestMethod]
        public void TestAlgorithmScenarioStationPreferance()
        {
            var algorithm = new KostynianOleksandrAlgorithm();
            var map = new Map();
            var robots = new List<Robot.Common.Robot>() {
                new Robot.Common.Robot() { Energy = 5000, Position = new Position(1, 1) },
                new Robot.Common.Robot() { Energy = 200, Position = new Position(2, 4) },
                new Robot.Common.Robot() { Energy = 1000, Position = new Position(3, 2) }
            };
            map.Stations.Add(new EnergyStation() { Position = new Position(1, 1), Energy = 800, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Position = new Position(2, 2), Energy = 200, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Position = new Position(3, 3), Energy = 5000, RecoveryRate = 2 });
            var act = algorithm.DoStep(robots, 1, map);
            var exp = new MoveCommand()
            {
                NewPosition = map.Stations[2].Position,
                Description = "moved to energized station"
            };
            Assert.IsTrue(act is MoveCommand);
            Assert.AreEqual(exp.Description, act.Description);
        }

        [TestMethod]
        public void TestAlgorithmScenarioRange()
        {
            var algorithm = new KostynianOleksandrAlgorithm();
            var map = new Map();
            var robots = new List<Robot.Common.Robot>() {
                new Robot.Common.Robot() { Energy = 100, Position = new Position(1, 1) },
                new Robot.Common.Robot() { Energy = 200, Position = new Position(2, 3) },
                new Robot.Common.Robot() { Energy = 1000, Position = new Position(50, 50) }
            };
            map.Stations.Add(new EnergyStation() { Position = new Position(1, 1), Energy = 800, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Position = new Position(6, 2), Energy = 200, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Position = new Position(40, 30), Energy = 5000, RecoveryRate = 2 });
            var act = algorithm.DoStep(robots, 1, map);
            var exp = new MoveCommand()
            {
                Description = "moved to nearest station"
            };
            Assert.IsTrue(act is MoveCommand);
            Assert.AreEqual(exp.Description, act.Description);
        }

        [TestMethod]
        public void TestAlgorithmScenarioOnStation()
        {
            var algorithm = new KostynianOleksandrAlgorithm();
            var map = new Map();
            var robots = new List<Robot.Common.Robot>() {
                new Robot.Common.Robot() { Energy = 100, Position = new Position(1, 1) },
                new Robot.Common.Robot() { Energy = 300, Position = new Position(2, 3) },
                new Robot.Common.Robot() { Energy = 200, Position = new Position(20, 20) },
                new Robot.Common.Robot() { Energy = 2000, Position = new Position(23, 20) }
            };
            map.Stations.Add(new EnergyStation() { Position = new Position(1, 1), Energy = 3000, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Position = new Position(2, 3), Energy = 200, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Position = new Position(4, 5), Energy = 2000, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Position = new Position(20, 20), Energy = 100, RecoveryRate = 2 });
            var act = algorithm.DoStep(robots, 1, map);
            var act2 = algorithm.DoStep(robots, 0, map);
            var act3 = algorithm.DoStep(robots, 2, map);
            var exp = new MoveCommand()
            {
                Description = "to station from station"
            };
            var exp3 = new MoveCommand()
            {
                Description = "robot attacked from station"
            };
            Assert.IsTrue(act2 is CollectEnergyCommand);
            Assert.AreEqual(exp.Description, act.Description);
            Assert.AreEqual(exp3.Description, act3.Description);
        }
    }
}

