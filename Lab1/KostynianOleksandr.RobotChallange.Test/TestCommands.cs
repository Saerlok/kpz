﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace KostynianOleksandr.RobotChallange.Test
{
    [TestClass]
    public class TestCommands
    {
        [TestMethod]
        public void TestMoveCommand()
        {
            var map = new Map();
            var algorithm = new KostynianOleksandrAlgorithm();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition,
                RecoveryRate =  2});
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 200,
                Position = new Position(2, 3)}};
            var command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition, stationPosition);
        }

        [TestMethod]
        public void TestCollectCommand()
        {
            var map = new Map();
            var algorithm = new KostynianOleksandrAlgorithm();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = stationPosition,
                RecoveryRate = 2
            });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 200,
                Position = new Position(1, 1)}};
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CollectEnergyCommand);
        }

        [TestMethod]
        public void TestCreateRobotCommand()
        {
            var algorithm = new KostynianOleksandrAlgorithm();
            var map = new Map();
            
            map.Stations.Add(new EnergyStation() { Position = new Position(1, 1), Energy = 400, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Position = new Position(2, 2), Energy = 200, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 700, Position = new Position(4, 5) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CreateNewRobotCommand);
        }
    }
}
