﻿using System;
using System.Collections.Generic;
using System.Text;
using Robot.Common;


namespace KostynianOleksandr.RobotChallange
{
    public class KostynianOleksandrAlgorithm : IRobotAlgorithm
    {
        public string Author => "Kostynian Oleksandr";

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            int pd = 50;
            int sto = 100;
            int stop = 150;
            int dvs = 200;
            //Position finalPosition = new Position();
            if ((movingRobot.Energy > 600) && (robots.Count < map.Stations.Count))
            {
                return new CreateNewRobotCommand() {NewRobotEnergy = 200 };
            }

            EnergyStation stationPosition = FindNearestFreeStation(robots[robotToMoveIndex], map, robots);

            if (stationPosition == null) {
                Robot.Common.Robot bestRobotToAttack = FindRobotToAttack(robots[robotToMoveIndex], map, robots);
                
                if (bestRobotToAttack != null && (DistanceHelper.FindDistance(movingRobot.Position, bestRobotToAttack.Position) + 70) <
                        (bestRobotToAttack.Energy / 10))
                {
                    return new MoveCommand() { NewPosition = bestRobotToAttack.Position };
                }

                return null;
            }

            if (stationPosition.Position == movingRobot.Position)
            {
                Robot.Common.Robot bestRobotToAttack = FindRobotToAttack(robots[robotToMoveIndex], map, robots);
                
                EnergyStation bestEnergyStationToGo = FindStationToGo(robots[robotToMoveIndex], map, robots);
                if (bestRobotToAttack != null && stationPosition.Energy < ((bestRobotToAttack.Energy / 10) -
                       DistanceHelper.FindDistance(bestRobotToAttack.Position, movingRobot.Position) - pd)
                       && bestEnergyStationToGo.Energy - DistanceHelper.FindDistance(bestEnergyStationToGo.Position,
                       movingRobot.Position) < ((bestRobotToAttack.Energy / 10) -
                       DistanceHelper.FindDistance(bestRobotToAttack.Position, movingRobot.Position) - pd)
                       && DistanceHelper.FindDistance(bestRobotToAttack.Position,
                       movingRobot.Position) < movingRobot.Energy - pd)
                {
                    return new MoveCommand() { NewPosition = bestRobotToAttack.Position,
                        Description = "robot attacked from station" };
                }    
                else if (bestEnergyStationToGo != null && stationPosition.Energy < bestEnergyStationToGo.Energy -
                     DistanceHelper.FindDistance(bestEnergyStationToGo.Position,
                     movingRobot.Position) - pd 
                     && DistanceHelper.FindDistance(bestEnergyStationToGo.Position,
                     movingRobot.Position) < movingRobot.Energy - pd)
                {
                    return new MoveCommand() { NewPosition = bestEnergyStationToGo.Position,
                        Description = "to station from station"};
                }
                else return new CollectEnergyCommand();
            }

            else
            {
                EnergyStation bestEnergyStationToGo = FindStationToGo(robots[robotToMoveIndex], map, robots);
                if (bestEnergyStationToGo != null &&
                    bestEnergyStationToGo.Energy*0.1 > DistanceHelper.FindDistance(bestEnergyStationToGo.Position,
                     movingRobot.Position) + 20
                     && DistanceHelper.FindDistance(bestEnergyStationToGo.Position,
                     movingRobot.Position) < movingRobot.Energy - sto)
                {
                    return new MoveCommand() { NewPosition = bestEnergyStationToGo.Position,
                        Description = "moved to energized station"
                    };
                }

                Robot.Common.Robot bestRobotToAttack = FindRobotToAttack(robots[robotToMoveIndex], map, robots);

                if ((bestRobotToAttack != null) && stationPosition.Energy < ((bestRobotToAttack.Energy / 10) -
                            DistanceHelper.FindDistance(bestRobotToAttack.Position, movingRobot.Position) - stop)
                            && DistanceHelper.FindDistance(bestRobotToAttack.Position, 
                            movingRobot.Position) < movingRobot.Energy - pd)
                {
                    return new MoveCommand() { NewPosition = bestRobotToAttack.Position,
                        Description = "robot attacked"};
                }
                else
                {
                    if(DistanceHelper.FindDistance(stationPosition.Position,
                            movingRobot.Position) < movingRobot.Energy)
                        return new MoveCommand() { NewPosition = stationPosition.Position, 
                        Description = "moved to nearest station" };
                    else
                    {
                        Position pos = new Position();
                        pos.X = (movingRobot.Position.X + stationPosition.Position.X) / 2;
                        pos.Y = (movingRobot.Position.Y + stationPosition.Position.Y) / 2;
                        return new MoveCommand() { NewPosition = pos };
                    }
                }
            }
        }

        public EnergyStation FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map,
                IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots))
                {
                    int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }
            return nearest == null ? null : nearest;
        }
        public bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot,
                IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }
        public bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                        return false;
                }
            }
            return true;
        }

        public Position MoveToStation(EnergyStation station, Robot.Common.Robot robot)
        {
            if (DistanceHelper.FindDistance(station.Position, robot.Position) < robot.Energy / 2 &&
                station.Energy >= robot.Energy)
            {
                return station.Position;
            }
            else if (DistanceHelper.FindDistance(station.Position, robot.Position) < robot.Energy / 2 &&
                station.Energy <= robot.Energy)
            {
                var position = new Position();
                position.X = (robot.Position.X + station.Position.X) / 2;
                position.Y = (robot.Position.Y + station.Position.Y) / 2;
                return position;
            }
            else
            {
                var position = new Position();
                position.X = (robot.Position.X + station.Position.X) / 2;
                position.Y = (robot.Position.Y + station.Position.Y) / 2;
                return position;
            }
        }

        public Position AttackEnemyRobot(Robot.Common.Robot myRobot,
            Robot.Common.Robot enemyRobot)
        {
            if (DistanceHelper.FindDistance(myRobot.Position, enemyRobot.Position) < enemyRobot.Energy * 0.1)
            {
                return enemyRobot.Position;
            }
            else
            {
                return myRobot.Position;
            }
        }

        public EnergyStation FindEnergizedStation(Map map)
        {
            EnergyStation finalStation = new EnergyStation();
            for (int i = 0; i < map.Stations.Count; i++)
            {
                if (map.Stations[i].Energy > finalStation.Energy)
                {
                    finalStation = map.Stations[i];
                }
            }
            return finalStation;
        }

        public Robot.Common.Robot FindRobotToAttack(Robot.Common.Robot movingRobot, Map map,
                IList<Robot.Common.Robot> robots)
        {
                Robot.Common.Robot bestRobotToAttack = new Robot.Common.Robot()
                { Energy = 0, Position = movingRobot.Position };
            foreach (var robot in robots)
            {                   
                        int d = DistanceHelper.FindDistance(robot.Position, movingRobot.Position);
                        if (((robot.Energy / 10) - d - 30) > ((bestRobotToAttack.Energy / 10) - 
                                DistanceHelper.FindDistance(bestRobotToAttack.Position, movingRobot.Position) - 30) && 
                                movingRobot.Energy > (d + 31)
                                && robot.OwnerName != "Kostynian Oleksandr")
                        {
                            bestRobotToAttack = robot;
                        }
            }

            return bestRobotToAttack.Energy == 0 ? null : bestRobotToAttack;
        }

        public EnergyStation FindStationToGo(Robot.Common.Robot movingRobot, Map map,
                IList<Robot.Common.Robot> robots)
        {
            EnergyStation bestEnergyStationToGo = new EnergyStation() 
                    { Position = new Position(1, 1), Energy = 0, RecoveryRate = 2 };
            foreach (var station in map.Stations)
            {
                int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                if (station.Energy - d > bestEnergyStationToGo.Energy -
                        DistanceHelper.FindDistance(bestEnergyStationToGo.Position, movingRobot.Position) 
                        && movingRobot.Energy > d 
                        && IsStationFree(station, movingRobot, robots))
                {
                    bestEnergyStationToGo = station;
                }
            }

            return bestEnergyStationToGo.Energy == 0 ? null : bestEnergyStationToGo;
        }
    }
}
