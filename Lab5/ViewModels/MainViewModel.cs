﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

using SampleMVVM.Commands;
using System.Collections.ObjectModel;
using SampleMVVM.Models;

namespace SampleMVVM.ViewModels
{
    class MainViewModel : ViewModelBase
    {
        public ObservableCollection<TicketBookingViewModel> TicketBookingList { get; set; } 

        #region Constructor

        public MainViewModel(List<TicketBooking> books)
        {
            TicketBookingList = new ObservableCollection<TicketBookingViewModel>(books.Select(b => new TicketBookingViewModel(b)));
        }

        #endregion
    }
}
