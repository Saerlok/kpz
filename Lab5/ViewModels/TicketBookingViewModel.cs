﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SampleMVVM.Models;
using SampleMVVM.Commands;
using System.Windows.Input;
using System.Media;

namespace SampleMVVM.ViewModels
{
    class TicketBookingViewModel : ViewModelBase
    {
        public TicketBooking TicketBooking;

        public TicketBookingViewModel(TicketBooking NewTicketBook)
        {
            this.TicketBooking = NewTicketBook;
        }

        public string EmployeeName
        {
            get { return TicketBooking.EmployeeName; }
            set
            {
                TicketBooking.EmployeeName = value;
                OnPropertyChanged("EmployeeName");
            }
        }

        public string ClientName
        {
            get { return TicketBooking.ClientName; }
            set
            {
                TicketBooking.ClientName = value;
                OnPropertyChanged("ClientName");
            }
        }

        public int TicketID
        {
            get { return TicketBooking.TicketID; }
            set
            {
                TicketBooking.TicketID = value;
                OnPropertyChanged("TicketID");
            }
        }

        public int Price
        {
            get { return TicketBooking.Price; }
            set
            {
                TicketBooking.Price = value;
                OnPropertyChanged("Price");
            }
        }

        #region Commands

        #region 
        private DelegateCommand clearCommand;

        public ICommand ClearCommand
        {
            get
            {
                if (clearCommand == null)
                {
                    clearCommand = new DelegateCommand(Clear, CanClear);
                }
                return clearCommand;
            }
        }

        private void Clear()
        {
            EmployeeName = "";
            ClientName = "";
            TicketID = 0;
            Price = 0;
        }

        private bool CanClear()
        {
            return TicketID > 0 || Price > 0 || EmployeeName != "" || ClientName != "";
        }

        #endregion

        #region 
        private DelegateCommand hehePlayCommand;

        public ICommand HehePlayCommand
        {
            get
            {
                if (hehePlayCommand == null)
                {
                    hehePlayCommand = new DelegateCommand(HehePlay);
                }
                return hehePlayCommand;
            }
        }

        private void HehePlay()
        {
            System.Reflection.Assembly assembly =
                System.Reflection.Assembly.GetExecutingAssembly();
            System.IO.Stream resourceStream =
                assembly.GetManifestResourceStream(@"SampleMVVM.ViewModels.HEHE_BOI.wav");
            SoundPlayer player = new SoundPlayer(resourceStream);
            player.Play();

            //string file = "HEHE_BOI.wav";
            //var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            //var stream = assembly.GetManifestResourceStream(string.Format("{0}.Resources.{1}", 
            //    assembly.GetName().Name, file));
            //var player = new System.Media.SoundPlayer(stream);
            //player.Play();
        }

        #endregion

        #endregion
    }
}
