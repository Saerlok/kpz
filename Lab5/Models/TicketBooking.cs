﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleMVVM.Models
{
    class TicketBooking
    {
        public string EmployeeName { get; set; }
        public string ClientName { get; set; }
        public int TicketID { get; set; }
        public int Price { get; set; }

        public TicketBooking(string employeeName, string clientName, int ticket, int price)
        {
            this.EmployeeName = employeeName;
            this.ClientName = clientName;
            this.TicketID = ticket;
            this.Price = price;
        }
    }
}
