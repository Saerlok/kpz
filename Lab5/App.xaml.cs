﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using SampleMVVM.Models;
using SampleMVVM.ViewModels;
using SampleMVVM.Views;

namespace SampleMVVM
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void OnStartup(object sender, StartupEventArgs e)
        {
            List<TicketBooking> ticketBookings = new List<TicketBooking>()
            {
                new TicketBooking("Genadiy", "Oleg", 1000, 250),
                new TicketBooking("Yoggan Sebastian", "Misha Jackson", 1001, 120),
                new TicketBooking("Lul", "C.J. Costava", 1002, 340),
                new TicketBooking("Halo Bronislavovich", "Mercure", 1003, 340)
            };

            MainView view = new MainView();
            MainViewModel viewModel = new ViewModels.MainViewModel(ticketBookings); 
            view.DataContext = viewModel;
            view.Show();
        }
    }
}
